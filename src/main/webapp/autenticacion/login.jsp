<jsp:include page="../plantilla/cabecera.jsp"></jsp:include>

<%@page import="org.ceti.proyecto.beans.Persona"%>
<%@page import="java.util.ArrayList"%>

<!-- Contenido de mi p�gina -->
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8 col-lg-6 col-xl-6">
                <div class="card">
                    <!-- primary, secondary, success, warning, danger, light, dark, default -->
                    <div class="card-header text-white bg-secondary">
                        Iniciar sesi�n
                    </div>
                    <div class="card-body">
                        <form method="post" action="${urlAplicacion}login">
                            <div class="form-group">
                                <label class="">Correo electr�nico</label>
                                <input type="email" name="email" class="form-control" placeholder="Ingresa tu email" />
                            </div>
                            <div class="form-group">
                                <label class="">Contrase�a</label>
                                <input type="password" name="password" class="form-control" placeholder="Ingresar tu clave" />
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary float-right" type="submit">
                                    <i class="fas fa-user"></i> Ingresar
                                </button>
                            </div>
                        </form>					
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- Contenido de la pagina -->
<jsp:include page="../plantilla/piepagina.jsp"></jsp:include>
