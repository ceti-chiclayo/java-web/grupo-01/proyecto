<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Proyecto Java Web - CETI</title>
<!-- Traer estilos de boostrap -->
<link type="text/css" rel="stylesheet" href="static/css/bootstrap.min.css" >
<!-- Archivo de estilos propios -->
<link type="text/css" rel="stylesheet" href="static/css/app.css">
<!-- Traer tipo de fuente de google -->
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,600">
</head>
<body>

<div class="flex-center position-ref full-height">
	<!-- Menú superior -->
	<div class="top-right links">
		<a href="admin">Inicio</a>
		<a href="login">Iniciar sesión</a>
		<a href="registrar">Registrarse</a>
	</div>
	
	<!-- Titulo de mi aplicación -->
	<div class="content">
		<div class="title m-b-md">Aplicación del Curso Java Web</div>
	</div>
</div>


<!-- Traer archivos javascript para Bootstrap -->
<script type="text/javascript" src="static/js/jquery-3.5.1.slim.min.js"></script>
<script type="text/javascript" src="static/js/popper.min.js"></script>
<script type="text/javascript" src="static/js/bootstrap.min.js"></script>
</body>
</html>