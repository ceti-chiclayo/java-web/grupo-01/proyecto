<%@page import="org.ceti.proyecto.beans.Persona"%>
<jsp:include page="../plantilla/cabecera.jsp"></jsp:include>
    <!-- Contenido de mi p�gina -->
<%
    Persona persona = (Persona) request.getAttribute("persona");
%>
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <!-- primary, secondary, success, warning, danger, light, dark, default -->
                    <div class="card-header text-white bg-secondary">
                        Modificar persona<a class="btn btn-sm btn-primary float-right"
                                            href="${urlAplicacion}persona">Regresar</a>
                    </div>
                    <div class="card-body">
                        <form id="formulario-persona" action="${urlAplicacion}persona/editar" method="POST">
                            <input type="hidden" name="id" value="<%=persona.getId()%>"/>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Apellido paterno</label>
                                <div class="col-sm-8">
                                    <input class="form-control" value="<%=persona.getApellido_paterno()%>" type="text" name="apellido_paterno"
                                           id="apellido_paterno" required autofocus />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Apellido materno</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="<%=persona.getApellido_materno()%>" name="apellido_materno"
                                           id="apellido_materno" required />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nombre</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="<%=persona.getNombre()%>" name="nombre"
                                           id="nombre" required />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" class="form-control" value="<%=persona.getEmail()%>" name="email"
                                           id="email" required />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Celular</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" value="<%=persona.getCelular()%>" name="celular"
                                           id="celular" />
                                </div>
                            </div>
                            <div class="from-group row">
                                <div class="col-sm-8 offset-sm-4">
                                    <button class="btn btn-sm btn-success" type="submit">Modificar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        $("#formulario-persona").validate();
        $("#apellido_paterno").rules(
                "add", {
                    required: true,
                    messages: {
                        required: "El apellido paterno es requerido",
                        minlength: "Por favor ingresar al menos 15 caracteres",
                        maxlength: "No debe exceder los 30 caracteres"
                    }
                }
        );
        $("#email").rules(
                "add", {
                    required: true,
                    email: true
                }
        )
    });
</script>
<jsp:include page="../plantilla/piepagina.jsp"></jsp:include>