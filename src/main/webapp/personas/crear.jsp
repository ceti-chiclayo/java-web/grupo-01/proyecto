<%@page import="org.ceti.proyecto.beans.Persona"%>
<jsp:include page="../plantilla/cabecera.jsp"></jsp:include>
    <!-- Contenido de mi p�gina -->
<%
    String apellido_paterno = "";
    String apellido_materno = "";
    String nombre = "";
    String celular = "";
    String email = "";
    if (request.getAttribute("persona") != null) {
        Persona persona = (Persona) request.getAttribute("persona");
        apellido_paterno = persona.getApellido_paterno();
        apellido_materno = persona.getApellido_materno();
        nombre = persona.getNombre();
        email = persona.getEmail();
        celular = persona.getCelular();
    }
    HttpSession sesion = request.getSession();
    String mensaje = "";
    if (sesion.getAttribute("mensaje") != null) {
        mensaje = (String) sesion.getAttribute("mensaje");
        sesion.removeAttribute("mensaje");
    }
%>
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <!-- primary, secondary, success, warning, danger, light, dark, default -->
                    <h4 class="card-header text-white bg-secondary">
                        Registrar persona<a class="btn btn-primary float-right"
                                            href="${urlAplicacion}persona"><i class="fas fa-redo"></i> Regresar</a>
                    </h4>
                    <div class="card-body">
                        <%                            
                            if (!mensaje.isEmpty()) {
                        %>
                        <div class="alert alert-danger" role="alert">
                            <%=mensaje%>
                        </div>
                        <%
                            }
                        %>
                        <form id="formulario-persona" action="${urlAplicacion}persona/crear" method="POST">
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Apellido paterno</label>
                                <div class="col-sm-8">
                                    <input class="form-control" value="<%=apellido_paterno%>" type="text" name="apellido_paterno" id="apellido_paterno" required autofocus />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Apellido materno</label>
                                <div class="col-sm-8">
                                    <input type="text" value="<%=apellido_materno%>" class="form-control" name="apellido_materno" id="apellido_materno" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Nombre</label>
                                <div class="col-sm-8">
                                    <input type="text" value="<%=nombre%>" class="form-control" name="nombre" id="nombre" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" value="<%=email%>" class="form-control" name="email" id="email" required/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Celular</label>
                                <div class="col-sm-8">
                                    <input type="text" value="<%=celular%>" class="form-control" name="celular" id="celular"/>
                                </div>
                            </div>
                            <div class="from-group row">
                                <div class="col-sm-8 offset-sm-4">
                                    <button class="btn btn-success" type="submit">
                                        <i class="fas fa-check-circle"></i> Registrar
                                    </button>
                                </div>
                            </div>
                        </form>						
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        $("#formulario-persona").validate();
        $("#apellido_paterno").rules(
                "add", {
                    required: true,
                    messages: {
                        required: "El apellido paterno es requerido",
                        minlength: "Por favor ingresar al menos 15 caracteres",
                        maxlength: "No debe exceder los 30 caracteres"
                    }
                }
        );
        $("#email").rules(
                "add", {
                    required: true,
                    email: true
                }
        )
    });
</script>

<jsp:include page="../plantilla/piepagina.jsp"></jsp:include>