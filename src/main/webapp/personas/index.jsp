<jsp:include page="../plantilla/cabecera.jsp"></jsp:include>

<%@page import="org.ceti.proyecto.beans.Persona"%>
<%@page import="java.util.ArrayList"%>
<%
    ArrayList<Persona> lista = (ArrayList<Persona>) request.getAttribute("listado");
    HttpSession sesion = request.getSession();
    String mensaje = "";
    if (sesion.getAttribute("mensaje") != null) {
        mensaje = (String) sesion.getAttribute("mensaje");
        sesion.removeAttribute("mensaje");
    }
%>
<!-- Contenido de mi p�gina -->
<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <!-- primary, secondary, success, warning, danger, light, dark, default -->
                    <h4 class="card-header text-white bg-secondary">
                        M�dulo de personas<a class="btn btn-success float-right"
                                             href="${urlAplicacion}persona/crear"><i class="fas fa-user"></i> Registar</a>
                    </h4>
                    <div class="card-body">
                        <%                            
                            if (!mensaje.isEmpty()) {
                        %>
                        <div class="alert alert-success" role="alert">
                            <%=mensaje%>
                        </div>
                        <%
                            }
                        %>

                        <table id="tabla-persona" class="display">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Apellido paterno</th>
                                    <th>Apellido materno</th>
                                    <th>Email</th>
                                    <th>Operaciones</th>
                                    <th>Operaciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%            
                                    for (Persona objPersona : lista) {
                                %>
                                <tr>
                                    <td><%=objPersona.getNombre()%></td>
                                    <td><%=objPersona.getApellido_paterno()%></td>
                                    <td><%=objPersona.getApellido_materno()%></td>
                                    <td><%=objPersona.getEmail()%></td>
                                    <td>
                                        <a href="${urlAplicacion }persona/editar?id=<%=objPersona.getId()%>" class="btn btn-sm btn-warning">
                                            <i class="far fa-edit"></i> Editar
                                        </a>
                                    </td>
                                    <td class="text-center">
                                        <form method="post" action="${urlAplicacion}persona/eliminar">
                                            <input type="hidden" name="id" value="<%=objPersona.getId()%>" />
                                            <button type="submit" onclick="return confirm('�Desea eliminar el registro?')" class="btn btn-sm btn-danger">
                                                <i class="fas fa-trash"></i> Eliminar
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>						
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(document).ready(function () {
        $('#tabla-persona').DataTable({
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ning�n dato disponible en esta tabla",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "�ltimo",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
        });
        
    });
</script>
<!-- Contenido de la pagina -->
<jsp:include page="../plantilla/piepagina.jsp"></jsp:include>
