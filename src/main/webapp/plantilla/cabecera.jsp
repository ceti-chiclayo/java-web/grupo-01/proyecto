<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Panel de administración - CETI</title>
<!-- Traer estilos de boostrap -->
<link type="text/css" rel="stylesheet"
	href="${urlAplicacion}static/css/bootstrap.min.css">
<!-- Traer tipo de fuente de google -->
<link type="text/css" rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Nunito:200,600">

<!-- Estilos para Datatables -->
<link type="text/css" rel="stylesheet"
	href="${urlAplicacion}static/datatables/datatables.min.css">

<!-- CSS para iconos -->
<link type="text/css" rel="stylesheet"
	href="${urlAplicacion}static/iconos/css/all.min.css">

<!-- Traer archivos javascript para Bootstrap -->
<script type="text/javascript"
	src="${urlAplicacion}static/js/jquery-3.5.1.slim.min.js"></script>
<script type="text/javascript"
	src="${urlAplicacion}static/js/popper.min.js"></script>
<script type="text/javascript"
	src="${urlAplicacion}static/js/bootstrap.min.js"></script>
<!-- Javascript para Datatables -->
<script src="${urlAplicacion}static/datatables/datatables.min.js"></script>
<!-- Javascript Validación -->
<script src="${urlAplicacion}static/validacion/jquery.validate.min.js"></script>
<script
	src="${urlAplicacion}static/validacion/additional-methods.min.js"></script>
<script
	src="${urlAplicacion}static/validacion/localization/messages_es.js"></script>

<%
	String nombre_usuario = (String) session.getAttribute("nombre_usuario");
%>
<style>
label.error{
	color:red
}
</style>
</head>
<body>
	<div id="app">
		<!-- Menú superior -->
		<nav class="navbar navbar-expand-md navbar-dark bg-dark">
			<div class="container-fluid">
				<a class="navbar-brand" href="#">Proyecto</a>
				<!-- Boton para menu escondido en pantallas pequeñas -->
				<button class="navbar-toggler" type="button" data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent" aria-expanded="false"
					aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<!-- Menú de la parte izquierda -->
					<ul class="navbar-nav mr-auto">

					</ul>
					<!-- Menú de la parte derecha -->
					<%
						if (nombre_usuario != null) {
					%>
					<ul class="navbar-nav ml-auto">
						<li class="nav-item"><a class="nav-link"
							href="${urlAplicacion}persona">Personas</a></li>
						<li class="nav-item"><a class="nav-link"
							href="${urlAplicacion}cursos">Cursos</a></li>
						<li class="nav-item"><a class="nav-link"
							href="${urlAplicacion}matriculados">Matriculados</a></li>
						<li class="nav-item"><a class="nav-link"><%=nombre_usuario%></a></li>
					</ul>
					<%
						}
					%>

				</div>
			</div>
		</nav>