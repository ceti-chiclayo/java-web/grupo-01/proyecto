package org.ceti.proyecto.models;

import java.util.ArrayList;

import org.ceti.proyecto.beans.Persona;
import org.ceti.proyecto.dao.PersonaDao;

public class PersonaModel {

    public ArrayList<Persona> listar() {
        ArrayList<Persona> listado = new ArrayList<>();
        PersonaDao daoPersona = new PersonaDao();
        listado = daoPersona.listar();
        return listado;
    }

    public Persona obtenerPorId(int id) {
        PersonaDao daoPersona = new PersonaDao();
        return daoPersona.obtenerPorID(id);
    }

    public Boolean registrar(Persona persona) {
        PersonaDao daoPersona = new PersonaDao();
        return daoPersona.registrar(persona);
    }
    
    public Boolean actualizar(Persona persona) {
        PersonaDao daoPersona = new PersonaDao();
        return daoPersona.actualizar(persona);
    }
    
    public Boolean eliminar(Persona persona) {
        PersonaDao daoPersona = new PersonaDao();
        return daoPersona.eliminar(persona);
    }
}
