package org.ceti.proyecto.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	private static Conexion miconexion;
	public Connection conn = null;

	private Conexion() {
		try {
			String url = "jdbc:mysql://localhost:3306/proyecto_java?useSSL=false&serverTimezone=America/Lima";
			String usuario = "root";
			String password = "root";
			Class.forName("com.mysql.cj.jdbc.Driver");
			conn = DriverManager.getConnection(url, usuario, password);
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static synchronized Conexion getInstancia() {
		if (miconexion == null) {
			miconexion = new Conexion();
		}
		return miconexion;
	}

	public Connection getConnection() {
		return conn;
	}
}
