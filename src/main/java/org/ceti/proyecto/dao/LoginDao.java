package org.ceti.proyecto.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.ceti.proyecto.beans.Usuario;

public class LoginDao {
	private Connection conexion;
	
	public Usuario login(String email, String password) {
		Usuario usuarioLogeado =  null;
		try {
			this.conexion = Conexion.getInstancia().getConnection();
			String consulta = "SELECT * FROM usuarios WHERE email = ? and "
					+ "password = md5(?)";
			PreparedStatement preStat = this.conexion.prepareStatement(consulta);
			preStat.setObject(1, email);
			preStat.setObject(2, password);
			ResultSet resultado = preStat.executeQuery();
			while(resultado.next()) {
				usuarioLogeado = new Usuario();
				usuarioLogeado.setId(resultado.getInt("id"));
				usuarioLogeado.setEmail(resultado.getString("email"));
			}
		}catch(Exception error) {
			error.printStackTrace();
		}
		return usuarioLogeado;
	}
}
