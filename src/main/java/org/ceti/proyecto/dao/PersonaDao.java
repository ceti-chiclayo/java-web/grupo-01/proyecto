package org.ceti.proyecto.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.ceti.proyecto.beans.Persona;

public class PersonaDao {
    // listar, obtener, registrar, actualiza, eliminar

    private Connection conexion;

    public Persona obtenerPorID(int id) {
        Persona persona = new Persona();
        try {
            this.conexion = Conexion.getInstancia().getConnection();
            String consulta = "SELECT * FROM personas WHERE id = ?";
            PreparedStatement preStat = this.conexion.prepareStatement(consulta);
            preStat.setObject(1, id);
            ResultSet resultado = preStat.executeQuery();
            while (resultado.next()) {
                persona.setApellido_paterno(resultado.getString("apellido_paterno"));
                persona.setApellido_materno(resultado.getString("apellido_materno"));
                persona.setNombre(resultado.getString("nombre"));
                persona.setEmail(resultado.getString("email"));
                persona.setCelular(resultado.getString("celular"));
                persona.setId(resultado.getInt("id"));
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
        return persona;
    }

    public Boolean registrar(Persona persona) {
        Boolean registrado = false;
        try {
            this.conexion = Conexion.getInstancia().getConnection();
            String consulta = "INSERT INTO "
                    + "personas(apellido_paterno, apellido_materno, nombre, email, celular)"
                    + " VALUES(?, ?, ?, ?, ?)";
            PreparedStatement preparar = this.conexion.prepareStatement(consulta);
            preparar.setObject(1, persona.getApellido_paterno());
            preparar.setObject(2, persona.getApellido_materno());
            preparar.setObject(3, persona.getNombre());
            preparar.setObject(4, persona.getEmail());
            preparar.setObject(5, persona.getCelular());
            int resultado = preparar.executeUpdate();
            if (resultado == 1) {
                registrado = true;
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
        return registrado;
    }

    public Boolean actualizar(Persona persona) {
        Boolean actualizado = false;
        try {
            this.conexion = Conexion.getInstancia().getConnection();
            String consulta = "UPDATE personas SET apellido_paterno = ?, "
                    + "apellido_materno = ?, nombre = ?, email =?, celular = ? "
                    + " WHERE id = ?";
            PreparedStatement preparar = this.conexion.prepareStatement(consulta);
            preparar.setObject(1, persona.getApellido_paterno());
            preparar.setObject(2, persona.getApellido_materno());
            preparar.setObject(3, persona.getNombre());
            preparar.setObject(4, persona.getEmail());
            preparar.setObject(5, persona.getCelular());
            preparar.setObject(6, persona.getId());
            int resultado = preparar.executeUpdate();
            if (resultado == 1) {
                actualizado = true;
            }
        } catch (SQLException error) {
            System.out.println(error.getMessage());
        }
        return actualizado;
    }

    public Boolean eliminar(Persona persona) {
        Boolean eliminado = false;
        try {
            this.conexion = Conexion.getInstancia().getConnection();
            String consulta = "DELETE FROM personas WHERE id = ?";
            PreparedStatement preparar = this.conexion.prepareStatement(consulta);
            preparar.setObject(1, persona.getId());
            int resultado = preparar.executeUpdate();
            if (resultado == 1) {
                eliminado = true;
            }
        } catch (SQLException error) {
            System.out.println(error.getMessage());
        }
        return eliminado;
    }

    public ArrayList<Persona> listar() {
        ArrayList<Persona> listado = new ArrayList<>();
        try {
            conexion = Conexion.getInstancia().getConnection();
            String sql = "SELECT * FROM personas";
            PreparedStatement preStat = conexion.prepareCall(sql);
            ResultSet resultado = preStat.executeQuery();
            while (resultado.next()) {
                Persona objPersona = new Persona();
                objPersona.setId(resultado.getInt("id"));
                objPersona.setNombre(resultado.getString("nombre"));
                objPersona.setApellido_paterno(resultado.getString("apellido_paterno"));
                objPersona.setApellido_materno(resultado.getString("apellido_materno"));
                objPersona.setEmail(resultado.getString("email"));
                objPersona.setCelular(resultado.getString("celular"));
                listado.add(objPersona);
            }
        } catch (SQLException error) {
            error.printStackTrace();
        }
        return listado;
    }
}
