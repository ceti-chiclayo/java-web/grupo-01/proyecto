package org.ceti.proyecto.controllers.autenticacion;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ceti.proyecto.beans.Usuario;
import org.ceti.proyecto.dao.LoginDao;

/**
 * Servlet implementation class Login
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// utilizarlo para mostrar el formulario de LOGIN
		request.getRequestDispatcher("/autenticacion/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// validar el usuario y contraseña en la base de datos
		// recibir los parametros del formulario
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		
		LoginDao loginDao = new LoginDao();
		Usuario usuario = loginDao.login(email, password);
		
		if(usuario != null) {
			System.out.println("Usuario logeado");
			// crear la variables de session
		}else {
			System.out.println("Error en inicio de sesión");
			// retornarme al login
		}
		
	}

}
