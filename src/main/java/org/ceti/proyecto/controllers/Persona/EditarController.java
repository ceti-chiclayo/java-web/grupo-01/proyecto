package org.ceti.proyecto.controllers.Persona;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.ceti.proyecto.beans.Persona;
import org.ceti.proyecto.models.PersonaModel;

/**
 * Servlet implementation class EditarController
 */
public class EditarController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditarController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        // response.getWriter().append("Served at: ").append(request.getContextPath());
        int id = Integer.parseInt(request.getParameter("id"));
        PersonaModel modelPersona = new PersonaModel();
        Persona persona = modelPersona.obtenerPorId(id);
        request.setAttribute("persona", persona);
        request.getRequestDispatcher("/personas/editar.jsp").forward(request, response);
    }

    /**
     * @param request
     * @param response
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        String apellidoPaterno = request.getParameter("apellido_paterno");
        String apellidoMaterno = request.getParameter("apellido_materno");
        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String celular = request.getParameter("celular");
        Integer id = Integer.parseInt(request.getParameter("id"));

        PersonaModel modeloPersona = new PersonaModel();
        Persona persona = modeloPersona.obtenerPorId(id);
        persona.setApellido_paterno(apellidoPaterno);
        persona.setApellido_materno(apellidoMaterno);
        persona.setNombre(nombre);
        persona.setEmail(email);
        persona.setCelular(celular);
        Boolean actualizado = modeloPersona.actualizar(persona);
        if (actualizado == true) {
            HttpSession sesion = request.getSession();
            sesion.setAttribute("mensaje", "Actualizado correctamente");
            response.sendRedirect(request.getContextPath() + "/persona");
        } else {
            System.out.println("Error al registrar");
        }
    }

}
