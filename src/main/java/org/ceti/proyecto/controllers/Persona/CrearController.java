package org.ceti.proyecto.controllers.Persona;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.ceti.proyecto.beans.Persona;
import org.ceti.proyecto.models.PersonaModel;

/**
 * Servlet implementation class CrearController
 */
public class CrearController extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CrearController() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @param request
     * @param response
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/personas/crear.jsp").forward(request, response);
    }

    /**
     * @param request
     * @param response
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String apellidoPaterno = request.getParameter("apellido_paterno");
        String apellidoMaterno = request.getParameter("apellido_materno");
        String nombre = request.getParameter("nombre");
        String email = request.getParameter("email");
        String celular = request.getParameter("celular");
        Persona persona = new Persona();
        persona.setApellido_paterno(apellidoPaterno);
        persona.setApellido_materno(apellidoMaterno);
        persona.setNombre(nombre);
        persona.setEmail(email);
        persona.setCelular(celular);
        PersonaModel modeloPersona = new PersonaModel();
        if (modeloPersona.registrar(persona)) {
            HttpSession sesion = request.getSession();
            sesion.setAttribute("mensaje", "Registrado correctamente");
            response.sendRedirect(request.getContextPath() + "/persona");
        } else {
            request.setAttribute("persona", persona);
            HttpSession sesion = request.getSession();
            sesion.setAttribute("mensaje", "Error al momento de registrar");
            request.getRequestDispatcher("/personas/crear.jsp").forward(request, response);
        }
    }
    
}
