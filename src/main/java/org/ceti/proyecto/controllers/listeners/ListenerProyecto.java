package org.ceti.proyecto.controllers.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ListenerProyecto implements ServletContextListener {
	
	@Override
	public void contextInitialized(ServletContextEvent evento) {
		String urlAplicacion = "http://localhost:8080/proyecto/";
		evento.getServletContext().setAttribute("urlAplicacion", urlAplicacion);
		System.out.println("--------- Proyecto iniciado ----------");
	}
	
	@Override
	public void contextDestroyed(ServletContextEvent evento) {
		System.out.println("--------- Proyecto apagado ----------");
	}	
}
